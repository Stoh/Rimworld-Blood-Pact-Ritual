#!/bin/bash
DIR_NAME="Blood Pact Ritual"
SRC_DIR="../$DIR_NAME"
DEST_DIR="$HOME/steam/RimWorld/Mods/"

./release.sh
rm -rf "$DEST_DIR/$DIR_NAME"
cp -r "$SRC_DIR" "$DEST_DIR"
