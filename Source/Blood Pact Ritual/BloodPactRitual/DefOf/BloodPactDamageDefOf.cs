﻿using Verse;

namespace Blood_Pact_Ritual.BloodPactRitual.DefOf
{
    [RimWorld.DefOf]
    public class BloodPactDamageDefOf
    {
        public static DamageDef RitualBloodPactSymbolicInjury;
        public static DamageDef RitualBloodPactShared;
        public static DamageDef RitualBloodPactSharedTendable;
    }
}
