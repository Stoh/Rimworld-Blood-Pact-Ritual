#!/bin/bash
DEST_DIR="../Blood Pact Ritual"

rm -rf "$DEST_DIR"
mkdir "$DEST_DIR"

for content in "About" "Defs" "Languages" "Textures"
do
  cp -r "$content" "$DEST_DIR"
done

mkdir "$DEST_DIR/Assemblies"
cp "Assemblies/Blood Pact Ritual.dll" "$DEST_DIR/Assemblies"

echo "Done."
